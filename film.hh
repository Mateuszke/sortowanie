#include <iostream>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iomanip>
#include <chrono>
using namespace std;
bool SprawdzAlfabetycznie(string a, string b);
class Film{
    public:
    int numer;
    string tytul;
    float ocena;
    Film(){
        numer=0;
        tytul={0};
        ocena=0;
    }
    
};
bool operator > (Film& film1, Film& film2){
    if(film1.ocena>film2.ocena){return true;}
    else if(film1.ocena==film2.ocena){
        return SprawdzAlfabetycznie(film1.tytul,film2.tytul);
    }
    else{return false;}
}
bool operator < (Film& film1, Film& film2){
    if(film1.ocena<film2.ocena){return true;}
    else if(film1.ocena==film2.ocena){
        return !SprawdzAlfabetycznie(film1.tytul,film2.tytul);
    }
    else{return false;}
}
bool operator >= (Film& film1, Film& film2){
    if(film1.ocena>=film2.ocena){return true;}
    else{return false;}

}
bool operator <= (Film& film1, Film& film2){
    if(film1.ocena<=film2.ocena){return true;}
    else{return false;}
}
istream& operator >> (istream &wejscie, Film& arg){
    wejscie>>arg.numer;
    wejscie>>arg.tytul;
    wejscie>>arg.ocena;
    return wejscie;
}
ostream& operator <<(ostream &wyjscie, Film& arg){
    wyjscie<<arg.numer<<','<<arg.tytul<<','<<fixed<<setprecision(1)<<arg.ocena<<endl;
    return wyjscie;
}